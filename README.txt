The Parse API module serves as a helper module for other Drupal Parse modules.
You don't need this module unless another module requires it or you want to
develop a new Parse-based module.

To use this module, or any Parse services modules, you will need to have an
account and create an application at http://parse.com

The Parse PHP Library is provided by https://github.com/apotropaic/parse.com-php-library/