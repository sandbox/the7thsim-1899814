<?php

/**
 * @file
 * Admin settings form and OAuth authentication integration
 */

/**
 * Admin settings form
 */
function parse_api_admin_settings() {
  $form = array();

  $form['parse_api_appid'] = array(
    '#type' => 'textfield',
    '#title' => t('APP ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('parse_api_appid', ''),
  );

  $form['parse_api_masterkey'] = array(
    '#type' => 'textfield',
    '#title' => t('MASTER KEY'),
    '#required' => TRUE,
    '#default_value' => variable_get('parse_api_masterkey', ''),
  );

  $form['parse_api_restkey'] = array(
    '#type' => 'textfield',
    '#title' => t('REST KEY'),
    '#required' => TRUE,
    '#default_value' => variable_get('parse_api_restkey', ''),
  );

  $form['parse_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API URL'),
    '#required' => TRUE,
    '#default_value' => variable_get('parse_api_url', 'https://api.parse.com/1/'),
  );

  $form['#validate'][] = 'parse_api_admin_settings_validate';
  $form['#submit'][] = 'parse_api_admin_settings_submit';
  return system_settings_form($form);
}

/**
 * Validator for the admin settings form.
 */
function parse_api_admin_settings_validate($form, &$form_state) {
  $app_id = trim($form_state['values']['parse_api_appid']);
  $master_key = trim($form_state['values']['parse_api_masterkey']);
  $rest_key = trim($form_state['values']['parse_api_restkey']);
  $url = trim($form_state['values']['parse_api_url']);

  // Potential Additional validation here ...
}

/**
 * Submit handler for the admin settings form.
 */
function parse_api_admin_settings_submit($form, &$form_state) {
  
}
